package baza4t26112020;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Andrzej Gac
 * ZSMEiE w Toruniu
 * last update 12-02-2021
 */
public class OperacjeNaBazie {
    Connection conn = null;  
    PreparedStatement ptmt = null;
     ResultSet rs = null;
     Statement stmt = null;
      String[] kolumny = { "lp","nazwa", "producent", "data_sprzedazy", "cena", "waga" };
     JTable table = null;
     JScrollPane sp = null; 
        String lp="";
        String nazwa = "";
        String producent= "";
        String data_sprzedazy = "";
        String cena = "";
        String waga = "";
        DefaultTableModel model = new DefaultTableModel();
       
    public OperacjeNaBazie() {
    }
    //------------------------------
    public DefaultTableModel zawartoscBazy(){
         model.setColumnIdentifiers(kolumny);
        table = new JTable();
        table.setModel(model);
        table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        table.setFillsViewportHeight(true);

              
        try {
            
             polacz();
             stmt = conn.createStatement();
            rs = stmt.executeQuery("SELECT * from towary;");
            while (rs.next() && !rs.isAfterLast()) {
               lp=rs.getString("lp");
                nazwa=rs.getString("nazwa");
                producent=rs.getString("producent");
                data_sprzedazy=rs.getString("data_sprzedazy");
                cena=rs.getString("cena");
                waga=rs.getString("waga");
                model.addRow(new Object[]{lp, nazwa,producent, data_sprzedazy,cena,waga});
            }      
            conn.close();
         } catch (SQLException ex) {
             Logger.getLogger(Gui.class.getName()).log(Level.SEVERE, null, ex);
         }
        return model;
    }
    //
    public void polacz() throws SQLException {
     
        conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/sklep2?zeroDateTimeBehavior=CONVERT_TO_NULL", "andrzej", "123");
   
    }
    //
    public String[] szukaj(String[] dane) throws SQLException{


    String[] wynik = new String[5];

   
        polacz();
     ptmt = (PreparedStatement) conn.prepareStatement("select * from towary where nazwa=? and producent=? ");
     ptmt.setString(1, dane[0]);
     ptmt.setString(2, dane[1]);

    
   
 try{
     System.out.println("sql:"+ptmt);
     rs = ptmt.executeQuery();
     while(rs.next()) {
                        wynik[0]=rs.getString("nazwa");
                        wynik[1]=rs.getString("producent");
                        wynik[2]=rs.getString("data_sprzedazy");
                        wynik[3]=rs.getString("cena");
                        wynik[4]=rs.getString("waga");
          }
        rs.close();
        conn.close();
        }catch(Exception e){}
      return wynik;
 }
//---------------------------------------------
public DefaultTableModel szukaj2(String[] dane) throws SQLException{
        model.setColumnIdentifiers(kolumny);
        table = new JTable();
        table.setModel(model);
        table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        table.setFillsViewportHeight(true);

   
        polacz();
     ptmt = (PreparedStatement) conn.prepareStatement("select * from towary where nazwa=? and producent=? ");
     ptmt.setString(1, dane[0]);
     ptmt.setString(2, dane[1]);

    
   
 try{
     System.out.println("sql:"+ptmt);
     rs = ptmt.executeQuery();
    while (rs.next() && !rs.isAfterLast()) {
               lp=rs.getString("lp");
                nazwa=rs.getString("nazwa");
                producent=rs.getString("producent");
                data_sprzedazy=rs.getString("data_sprzedazy");
                cena=rs.getString("cena");
                waga=rs.getString("waga");
                model.addRow(new Object[]{lp, nazwa,producent, data_sprzedazy,cena,waga});
            } 
        rs.close();
        conn.close();
        }catch(Exception e){}
      return model;
 }
//--------------------------------------------
    public void modyfikuj(String[] mod){
    

        try {
            polacz();
            ptmt =  conn.prepareStatement("update towary set nazwa=?,producent=?,data_sprzedazy=?,cena=?,waga=? where lp=?;");
            ptmt.setString(1,  mod[0] );
            ptmt.setString(2,  mod[1] );
            ptmt.setString(3,  mod[2]  );
            ptmt.setString(4,  mod[3] );
            ptmt.setString(5,  mod[4] );
            ptmt.setString(6,  mod[5] );
           
            System.out.println("sql:"+ptmt);
            
            ptmt.executeUpdate();
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(OperacjeNaBazie.class.getName()).log(Level.SEVERE, null, ex);
        }
          
 }
    //---------------------------------------------
    public void dodaj(String[] dod){

        try {
            polacz();
            ptmt = (PreparedStatement) conn.prepareStatement("insert into towary(nazwa,producent,data_sprzedazy,cena,waga) values (?,?,?,?,?);");
            ptmt.setString(1,  dod[0] );
            ptmt.setString(2,  dod[1] );
            ptmt.setString(3,  dod[2] );
            ptmt.setString(4,  dod[3] );
            ptmt.setString(5,  dod[4] );
                        
            System.out.println("sql:"+ptmt);
            ptmt.executeUpdate();
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(OperacjeNaBazie.class.getName()).log(Level.SEVERE, null, ex);
        }
      
 }
    //----------- usuń --------------------
    public void usuwanie(String[] us){
            
        try {
            polacz();
            ptmt = (PreparedStatement) conn.prepareStatement("delete from towary where nazwa=? and producent=? and lp=?;");
            ptmt.setString(1,  us[0] );
            ptmt.setString(2,  us[1] );
            ptmt.setString(3,  us[2] );
             System.out.println("sql:"+ptmt);
            ptmt.executeUpdate();
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(OperacjeNaBazie.class.getName()).log(Level.SEVERE, null, ex);
        }
         
 }
    
    //-------------------------------------
}


