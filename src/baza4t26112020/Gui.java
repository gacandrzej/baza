package baza4t26112020;

import java.awt.Color;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.HeadlessException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JTextArea;


/*
* zadanie domowe termin długi
* 0) rozmieszczenie elementów, ręcznie c.setLayout(new GroupGridLayout());
* 1) okno logowania do aplikacji z autoryzacją w bazie
* 2) czy w netbeansach możemy łączyć się po ssl lub tls?
* 3) sygnalizacja że jesteśmy połączeni
* 4) sposób wyświetlenia danych: https://docs.oracle.com/javase/tutorial/uiswing/components/table.html
To będzie na lekcjach:
* 1) wyszukiwanie danych
* 2) modyfikacja danych
* 3) usuwanie danych
* 4) dodawanie danych
*/
/**
 *
 * @author Andrzej Gac
 * ZSMEiE w Toruniu
 * last update 18-12-2020
 */
public class Gui extends JFrame {
     Connection conn = null;          
     PreparedStatement ptmt = null;
     ResultSet rs = null;
     Statement stmt = null;
     JTextArea wyniki=null; 
     ArrayList results = new ArrayList();
    
    
    public Gui() throws HeadlessException {
         setSize(1000,600);
         setTitle("-----Aplikacja bazo-danowa--------");
         Container c = getContentPane();
         c.setLayout(new FlowLayout());
         c.setBackground(new Color(0, 102, 102));
         wyniki = new JTextArea(50, 50);
    
        GuiNowe gn = new GuiNowe();
        gn.setVisible(true);
        c.add(gn);
         setVisible(true);
          setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    } // koniec konstruktora
    
    
}// koniec klasy Gui
