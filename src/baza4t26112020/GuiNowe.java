package baza4t26112020;

import com.toedter.calendar.JDateChooser;
import java.awt.Color;
import java.awt.GridLayout;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.table.DefaultTableModel;


/**
 *
 * @author Andrzej Gac
 * ZSMEiE w Toruniu
 * last update 12-02-2021
 */
public class GuiNowe extends javax.swing.JPanel {
    DefaultTableModel model=null;
    OperacjeNaBazie o = new OperacjeNaBazie();
    JDateChooser data=null;
   // UtilDateModel dat = new UtilDateModel();
   // JDatePanelImpl datePanel = new JDatePanelImpl(dat);
   // JDatePickerImpl datePicker = new JDatePickerImpl(datePanel);
    public GuiNowe() {
        initComponents();
       data = new JDateChooser();
     // jPanel1.add(data);
javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(31, 31, 31)
                .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 26, Short.MAX_VALUE)
                .addComponent(data, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(data))
                .addContainerGap())
        );
       jbzawartoscBazy.setBackground(Color.red);
    }

   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jbszukaj = new javax.swing.JButton();
        jbdodaj = new javax.swing.JButton();
        jbmodyfikuj = new javax.swing.JButton();
        jbusuń = new javax.swing.JButton();
        jbzawartoscBazy = new javax.swing.JButton();
        producent = new javax.swing.JTextField();
        nazwa = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        cena = new javax.swing.JTextField();
        waga = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        try {
            jPanel1 =(javax.swing.JPanel)java.beans.Beans.instantiate(getClass().getClassLoader(), "baza4t26112020.GuiNowe_jPanel1");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (java.io.IOException e) {
            e.printStackTrace();
        }
        jLabel6 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel5 = new javax.swing.JLabel();
        lp = new javax.swing.JTextField();

        setBackground(new java.awt.Color(0, 102, 102));
        setAlignmentX(0.0F);
        setAlignmentY(0.0F);
        setPreferredSize(new java.awt.Dimension(1000, 600));

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(jTable1);

        jbszukaj.setText("szukaj");
        jbszukaj.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbszukajActionPerformed(evt);
            }
        });

        jbdodaj.setText("dodaj");
        jbdodaj.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jbdodajMouseClicked(evt);
            }
        });
        jbdodaj.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbdodajActionPerformed(evt);
            }
        });

        jbmodyfikuj.setText("modyfikuj");
        jbmodyfikuj.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jbmodyfikujMouseClicked(evt);
            }
        });
        jbmodyfikuj.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbmodyfikujActionPerformed(evt);
            }
        });

        jbusuń.setText("usuń");
        jbusuń.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jbusuńMouseClicked(evt);
            }
        });
        jbusuń.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbusuńActionPerformed(evt);
            }
        });

        jbzawartoscBazy.setText("zawartość bazy");
        jbzawartoscBazy.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbzawartoscBazyActionPerformed(evt);
            }
        });

        jLabel1.setText("producent:");

        jLabel2.setText("nazwa:");

        jLabel3.setText("cena:");

        jLabel4.setText("waga:");

        jLabel6.setForeground(new java.awt.Color(0, 0, 0));
        jLabel6.setText("data sprzedaży:");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(31, 31, 31)
                .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(178, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(22, Short.MAX_VALUE)
                .addComponent(jLabel6)
                .addContainerGap())
        );

        jLabel5.setText("lp:");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 794, Short.MAX_VALUE)
                        .addGap(18, 18, 18))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel3))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(132, 132, 132)
                                .addComponent(jLabel5)
                                .addGap(100, 100, 100)
                                .addComponent(lp, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel2)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(nazwa, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(40, 40, 40)
                                .addComponent(jLabel1))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(cena, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel4)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(producent, javax.swing.GroupLayout.DEFAULT_SIZE, 73, Short.MAX_VALUE)
                            .addComponent(waga))
                        .addGap(69, 69, 69)))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jbusuń)
                    .addComponent(jbmodyfikuj)
                    .addComponent(jbdodaj)
                    .addComponent(jbszukaj)
                    .addComponent(jbzawartoscBazy))
                .addGap(56, 56, 56))
            .addGroup(layout.createSequentialGroup()
                .addGap(52, 52, 52)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 884, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jbdodaj, jbmodyfikuj, jbszukaj, jbusuń, jbzawartoscBazy});

        layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {cena, nazwa, producent, waga});

        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(72, 72, 72)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jbszukaj)
                    .addComponent(producent, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(nazwa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2)
                    .addComponent(jLabel5)
                    .addComponent(lp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(31, 31, 31)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jbdodaj)
                            .addComponent(cena, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(waga, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel3)
                            .addComponent(jLabel4))
                        .addGap(18, 18, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(4, 4, 4)))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jbmodyfikuj)
                        .addGap(18, 18, 18)
                        .addComponent(jbusuń)
                        .addGap(18, 18, 18)
                        .addComponent(jbzawartoscBazy)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 391, Short.MAX_VALUE))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jbszukajActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbszukajActionPerformed
        try {
            String [] wyn=new String[5];
            String [] d=new String[5];
            d[0]=nazwa.getText();
            d[1]=producent.getText();
            wyn= o.szukaj(d);
            System.out.println("format daty:"+wyn[2]);
            data.setDateFormatString(wyn[2]);
            
            cena.setText(wyn[3]);
            waga.setText(wyn[4]);
            //------------------------------------------------
            wyczyscTabele();
            jTable1.setModel(o.szukaj2(d));
            model.setValueAt(77, 1, 5);
        } catch (SQLException ex) {
            Logger.getLogger(GuiNowe.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_jbszukajActionPerformed

    private void jbzawartoscBazyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbzawartoscBazyActionPerformed
        wyczyscTabele();
        jTable1.setModel(o.zawartoscBazy());
        jbzawartoscBazy.setBackground(Color.GREEN);
    }//GEN-LAST:event_jbzawartoscBazyActionPerformed
private void wyczyscTabele(){
     model = (DefaultTableModel )jTable1.getModel();
            int rows = model.getRowCount(); 
            for(int i = rows - 1; i >=0; i--)
            {
            model.removeRow(i); 
            }
            
}
    private void jbmodyfikujActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbmodyfikujActionPerformed
            String [] wyn=new String[6];
            String [] d=new String[6];
            d[0]=nazwa.getText();
            d[1]=producent.getText();
           //d[2]=data_sprzedazy.getText();
            SimpleDateFormat Date_Format = new SimpleDateFormat("yyyy-MM-dd"); 
           d[2]=Date_Format.format(data.getDate());
            d[3]=cena.getText();
            d[4]=waga.getText();
            d[5]=lp.getText(); // dla lp
            o.modyfikuj(d);
            wyczyscTabele();
            jbzawartoscBazyActionPerformed(evt);
    }//GEN-LAST:event_jbmodyfikujActionPerformed

    private void jbdodajActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbdodajActionPerformed
         String [] d=new String[5];
            d[0]=nazwa.getText();
            d[1]=producent.getText();
            //d[2]=data_sprzedazy.getText();
            SimpleDateFormat Date_Format = new SimpleDateFormat("yyyy-MM-dd"); 
           d[2]=Date_Format.format(data.getDate());
            d[3]=cena.getText();
            d[4]=waga.getText();
        o.dodaj(d);
        wyczyscTabele();
        jbzawartoscBazyActionPerformed(evt);
    }//GEN-LAST:event_jbdodajActionPerformed

    private void jbmodyfikujMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jbmodyfikujMouseClicked
        
    }//GEN-LAST:event_jbmodyfikujMouseClicked

    private void jbusuńActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbusuńActionPerformed
    // realizacja z klasą 4T 12 luty 2021
    String [] d=new String[3];
       d[0]=nazwa.getText();
       d[1]=producent.getText();
       d[2]=lp.getText(); // dla lp
       o.usuwanie(d);
       wyczyscTabele();
       jbzawartoscBazyActionPerformed(evt);
    }//GEN-LAST:event_jbusuńActionPerformed

    private void jbdodajMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jbdodajMouseClicked
       
    }//GEN-LAST:event_jbdodajMouseClicked

    private void jbusuńMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jbusuńMouseClicked
        
    }//GEN-LAST:event_jbusuńMouseClicked


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField cena;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTable jTable1;
    private javax.swing.JButton jbdodaj;
    private javax.swing.JButton jbmodyfikuj;
    private javax.swing.JButton jbszukaj;
    private javax.swing.JButton jbusuń;
    private javax.swing.JButton jbzawartoscBazy;
    private javax.swing.JTextField lp;
    private javax.swing.JTextField nazwa;
    private javax.swing.JTextField producent;
    private javax.swing.JTextField waga;
    // End of variables declaration//GEN-END:variables
}
